<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('table_bags', function (Blueprint $table) {
            $table->id();
            $table->text('bag');
            $table->text('veichle');
            $table->decimal('price_km');
            $table->integer('a_partire_da');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('table_bags');
    }
};
