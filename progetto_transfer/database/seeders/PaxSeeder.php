<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PaxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 1; $i <= 8; $i++) {
            if ($i <= 3) {
                DB::table('table_price_paxes')->insert([
                    'pax' => $i,
                    'veichle' => 'car',
                    'price_km' => 1,
                ]);
            } else {
                DB::table('table_price_paxes')->insert([
                    'pax' => $i,
                    'veichle' => 'van',
                    'price_km' => 1.5,
                ]);
            }
        }
    }
}
