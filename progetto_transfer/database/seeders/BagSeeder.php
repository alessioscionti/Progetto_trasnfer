<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class BagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('table_bags')->insert(
            [
                'bag' => 'valigia',
                'veichle' => 'car',
                'price_km' => 1,
                'a_partire_da' => 140,
            ]
        );
        DB::table('table_bags')->insert([
            'bag' => 'trolley',
            'veichle' => 'car',
            'price_km' => 1,
            'a_partire_da' => 140,
        ]);
        DB::table('table_bags')->insert([
            'bag' => 'valigia',
            'veichle' => 'van',
            'price_km' => 1.5,
            'a_partire_da' => 140,
        ]);
        DB::table('table_bags')->insert([
            'bag' => 'trolley',
            'veichle' => 'van',
            'price_km' => 1.5,
            'a_partire_da' => 140,
        ]);
    }
}
