<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class BikeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 1; $i <= 10; $i++) {
            if ($i <= 4) {
                DB::table('table_bikes')->insert([
                    'pax' => $i,
                    'veichle' => 'van',
                    'price_km' => 1,
                ]);
            } else {
                DB::table('table_bikes')->insert([
                    'pax' => $i,
                    'veichle' => 'vanCart',
                    'price_km' => 1.5,
                ]);
            }
        }
    }
}
