<x-layout>

    <style>
        .hiddenField {
            visibility: hidden;
        }
    </style>

    <div class="container">

        <div class="row justify-content-center ">

            <div class="mb-2 mt-4 row">
                <label class="col-sm-2 col-form-label">Numero Persone</label>
                <div class="col-sm-2">
                    <input type="text" id="pax" class="form-control" placeholder="1,2,3..">
                </div>
                <div class="col-sm-4">
                    <select id="transfer" class="form-select" aria-label="Indica il tipo di transfer">
                        <option selected>Indica il tipo di transfer</option>
                        <option value="1">Persone</option>
                        <option value="2">Persone + Bagagli</option>
                        <option value="3">Persone + Bici</option>
                        <option value="4">solo Bici</option>
                        <option value="5">solo Bagagli</option>
                    </select>
                </div>
                <div class="col-sm-4 text-end ">
                    <input type="button" class="btn btn-primary" value="AGGIUNGI TRATTA" onclick="aggiungi()">
                    <input type="button" class="btn btn-success" value="ELABORA PERCORSO" onclick="ElaborazioneDatiForm()">
                </div>
            </div>

            <div class="mb-5 mt-4 row">
                <div class="col-12">
                    <div class="card-body">
                        <!-- <div id="rispostaElobarazione" class="bg-warning bg-gradient" style="display: none"></div> -->
                        <div id="rispostaElobarazione" class="bg-info-50 bg-gradient">
                            <h2> Prezzo € 100.00 </h2>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="row justify-content-center">

            <div class="col-6 text-center">
                <div class="card-body">
                    <div class="form-group">
                        <label> Partenza </label>
                        <input type="text" id="luogoPar" class="form-control" placeholder="Indirizzo Partenza">
                        <input type="text" id="latPar" class="hiddenField">
                        <input type="text" id="lonPar" class="hiddenField">
                    </div>
                </div>
            </div>
            <div class="col-6 text-center">
                <div class="card-body">
                    <div class="form-group">
                        <label> Arrivo </label>
                        <input type="text" id="luogoArr" class="form-control" placeholder="Indirizzo Arrivo">
                        <input type="text" id="latArr" class="hiddenField">
                        <input type="text" id="lonArr" class="hiddenField">
                    </div>
                </div>
            </div>

            <div class="col-6 text-left">
                <div id="kmpercorso" class="bg-warning bg-gradient" style="display: none"></div>
            </div>
            <div class="col-6 text-left ">
                <input type="text" id="tratta" class="tratte hiddenField">
            </div>

        </div>


    </div>

    <script src="https://code.jquery.com/jquery-3.7.1.slim.js" integrity="sha256-UgvvN8vBkgO0luPSUl2s8TIlOSYRoGFAX4jlCIm9Adc=" crossorigin="anonymous"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD0nfgWwD2Ha8CEZDILlIuVMfAUv9vi-Bo&libraries=places&callback=initAutocomplete" async defer>
    </script>


    <script>
        // LA VARIABILE percorso VIENE UTILIZZATA PER DEFINIRE IN MODO UNIVOCO 
        // TUTTI GLI ELEMENTI CREATI IN MODO DINAMICO NEL DOM
        var percorso = 1;

        function aggiungi() {

            const NUMid = percorso;
            //console.log("OBJECT ID=" + NUMid);

            var html = `
            <div class="row justify-content-center">
                <div class="col-6 text-center mb-3">
                    <div class="card-body">
                        <div class="form-group">
                            <label > Partenza </label>
                            <input type="text" id="luogoPar` + NUMid + `" class="form-control" placeholder="Indirizzo Partenza">                        
                            <input type="text" id="latPar` + NUMid + `" class="hiddenField">
                            <input type="text" id="lonPar` + NUMid + `" class="hiddenField">    
                        </div>
                    </div>
                </div>
                <div class="col-6 text-center mb-3">
                    <div class="card-body">
                        <div class="form-group">
                            <label > Arrivo </label>
                            <input type="text" id="luogoArr` + NUMid + `" class="form-control" placeholder="Indirizzo Arrivo">                           
                            <input type="text" id="latArr` + NUMid + `" class="hiddenField">   
                            <input type="text" id="lonArr` + NUMid + `" class="hiddenField">
                        </div>   
                    </div>
                </div>                
                <div class="col-6 text-left mb-3">
                    <div id="kmpercorso` + NUMid + `" class="bg-warning bg-gradient" style="display: none"></div>
                </div>                
                <div class="col-6 text-left mb-3">
                    <input type="text" id="tratta` + NUMid + `" class="tratte hiddenField">
                </div>
            </div>
            `;

            // PROCEDURA DI CREAZIONE NEL DOM DEI CAMPI PER IL CALCOLO DELLA TRATTA AGGIUNTIVI
            var nuovoElemento = $(html);
            var contenitore = $(".container");
            contenitore.append(nuovoElemento);

            // recupera oggetti html per id
            var IdLuogoPar = 'luogoPar' + NUMid;
            var IdLuogoArr = 'luogoArr' + NUMid;
            var IdCalcola = 'calcola' + NUMid;
            var LatPar = '#latPar' + NUMid;
            var LonPar = '#lonPar' + NUMid;
            var LatArr = '#latArr' + NUMid;
            var LonArr = '#lonArr' + NUMid;
            var KmPercorso = '#kmpercorso' + NUMid;
            var Tratta = '#tratta' + NUMid;

            var inpPar = document.getElementById(IdLuogoPar);
            var inpArr = document.getElementById(IdLuogoArr);
            // associa funzioni maps ad oggetti html e associa a variabili
            var autocompletePar = new google.maps.places.Autocomplete(inpPar);
            var autocompleteArr = new google.maps.places.Autocomplete(inpArr);

            autocompletePar.addListener('place_changed', function() {
                var place = autocompletePar.getPlace();
                $(LatPar).val(place.geometry['location'].lat());
                $(LonPar).val(place.geometry['location'].lng());

                // initMap();
            });

            autocompleteArr.addListener('place_changed', function() {
                var place = autocompleteArr.getPlace();
                $(LatArr).val(place.geometry['location'].lat());
                $(LonArr).val(place.geometry['location'].lng());

                const partenza = {
                    lat: parseFloat($(LatPar).val()),
                    lng: parseFloat($(LonPar).val())
                };
                const destinazione = {
                    lat: parseFloat($(LatArr).val()),
                    lng: parseFloat($(LonArr).val())
                };

                const route = {
                    origin: partenza,
                    destination: destinazione,
                    travelMode: 'DRIVING'
                }

                let directionsService = new google.maps.DirectionsService();
                let directionsRenderer = new google.maps.DirectionsRenderer();

                directionsService.route(route, function(response, status) {
                    // anonymous function to capture directions

                    if (status !== 'OK') {
                        window.alert('Directions request failed due to ' + status + ' per oggetto tratta ' + NUMid);
                        return;

                    } else {
                        directionsRenderer.setDirections(response);
                        var directionsData = response.routes[0].legs[0];

                        if (!directionsData) {
                            window.alert('Directions request failed per oggetto tratta ' + NUMid);
                            return;
                        }

                        var distancekm = (directionsData.distance.value / 1000);
                        // I VALORI NEI CAMPI TRATTA SONO ESPRESSI IN METRI
                        // IN MODO DA TRATTARE LE SOMME DELLE TRATTE PIU FACILMENTE
                        // IN QUANTO LE DISTANZE SONO ESPRESSE CON NUMERI INTERI 
                        $(Tratta).val(directionsData.distance.value);
                        $(KmPercorso).text("Calcolo distanza della tratta in Km: " + distancekm);
                        $(KmPercorso).fadeIn();

                    }
                });

            });


            percorso++;

        }

        window.addEventListener('load', initialize)
        /* google.maps.event.addEventListener(window, 'load', initialize); */

        function initialize() {
            // recupera oggetti html per id
            var inpPar = document.getElementById('luogoPar');
            var inpArr = document.getElementById('luogoArr');
            // associa funzioni maps ad oggetti html e associa a variabili
            var autocompletePar = new google.maps.places.Autocomplete(inpPar);
            var autocompleteArr = new google.maps.places.Autocomplete(inpArr);

            autocompletePar.addListener('place_changed', function() {
                var place = autocompletePar.getPlace();
                $('#latPar').val(place.geometry['location'].lat());
                $('#lonPar').val(place.geometry['location'].lng());

                // initMap();
            });

            autocompleteArr.addListener('place_changed', function() {
                var place = autocompleteArr.getPlace();
                $('#latArr').val(place.geometry['location'].lat());
                $('#lonArr').val(place.geometry['location'].lng());

                initMap();
            });

        }

        function initMap() {

            const partenza = {
                lat: parseFloat($('#latPar').val()),
                lng: parseFloat($('#lonPar').val())
            };
            const destinazione = {
                lat: parseFloat($('#latArr').val()),
                lng: parseFloat($('#lonArr').val())
            };

            const route = {
                origin: partenza,
                destination: destinazione,
                travelMode: 'DRIVING'
            }

            let directionsService = new google.maps.DirectionsService();
            let directionsRenderer = new google.maps.DirectionsRenderer();

            directionsService.route(route, function(response, status) {
                // anonymous function to capture directions
                console.log(response.status);
                if (status !== 'OK') {
                    window.alert('Directions request failed due to ' + status);
                    return;

                } else {
                    directionsRenderer.setDirections(response);
                    var directionsData = response.routes[0].legs[0];

                    if (!directionsData) {
                        window.alert('Directions request failed');
                        return;
                    }

                    var distancekm = (directionsData.distance.value / 1000);
                    // I VALORI NEI CAMPI TRATTA SONO ESPRESSI IN METRI
                    // IN MODO DA TRATTARE LE SOMME DELLE TRATTE PIU FACILMENTE
                    // IN QUANTO LE DISTANZE SONO ESPRESSE CON NUMERI INTERI 
                    $("#tratta").val(directionsData.distance.value);
                    $('#kmpercorso').text("Calcolo distanza della tratta in Km: " + distancekm);
                    $('#kmpercorso').fadeIn();

                }
            });

        }

        function ElaborazioneDatiForm() {

            var numeroPax = $("#pax").val();
            var tipoTransfer = $("#transfer").val();
            var metriTotaliTratta = 0;
            var kmTotaliTratte = 0;

            $(".tratte").each(function(index, elem) {
                // console.log(elem.value);
                metriTotaliTratta = metriTotaliTratta + parseInt(elem.value);
            });

            kmTotaliTratte = (metriTotaliTratta / 1000);

            console.log(`numeropax= ${numeroPax} tipo transfer= ${tipoTransfer} km totali= ${kmTotaliTratte}`);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: "/calcolo",
                method: "POST",
                data: {
                    pax: numeroPax,
                    trf: tipoTransfer,
                    kmt: kmTotaliTratte
                },
                success: function(response) {
                    console.log(" RISPOSTA ajax " + response.calcolo);
                }

            });


        }
    </script>


</x-layout>