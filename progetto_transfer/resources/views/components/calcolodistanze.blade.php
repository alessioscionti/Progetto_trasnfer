
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-6 text-center">
                <div class="card-body">
                    <div class="form-group">
                        <label for="autocomplete"> Partenza </label>
                        <input type="text" name="autocomplete" id="autocomplete" class="form-control" placeholder="Select Location">
                    </div>
                    <div class="form-group" id="lat_area" hidden>
                        <label for="latitude"> Latitude </label>
                        <input type="text" name="latitude" id="latitude" class="form-control">
                    </div>
                    <div class="form-group" id="long_area" hidden>
                        <label for="latitude"> Longitude </label>
                        <input type="text" name="longitude" id="longitude" class="form-control">
                    </div>
                </div>
            </div>
            <div class="col-6 text-center">
                <div class="card-body">
                    <div class="form-group">
                        <label for="autocomplete"> Arrivo! </label>
                        <input type="text" name="autocomplete" id="autocomplete2" class="form-control" placeholder="Select Location">
                    </div>
                    <div class="form-group" id="lat_area" hidden>
                        <label for="latitude"> Latitude </label>
                        <input type="text" name="latitude" id="latitude2" class="form-control">
                    </div>
                    <div class="form-group" id="long_area" hidden>
                        <label for="latitude"> Longitude </label>
                        <input type="text" name="longitude" id="longitude2" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt-5">
            <div class="col-12 text-center">
                <div id="alert" style="display: none"></div>
            </div>
        </div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD0nfgWwD2Ha8CEZDILlIuVMfAUv9vi-Bo&libraries=places&callback=initAutocomplete"
            async defer>
    </script>
    
    <script>
        function initAutocomplete() {
	var map = new google.maps.Map(document.getElementById('autocomplete'), {});
	// Create the search box and link it to the UI element.
	var input = document.getElementById('pac-input');
	//console.log(input);
	var searchBox = new google.maps.places.SearchBox(input);
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	// Bias the SearchBox results towards current map's viewport.
	map.addListener('bounds_changed', function () {
		searchBox.setBounds(map.getBounds());
	});
}

window.addEventListener('load', initialize)
/* google.maps.event.addEventListener(window, 'load', initialize); */
function initialize() {
	var input = document.getElementById('autocomplete');
	var input2 = document.getElementById('autocomplete2');
	var autocomplete = new google.maps.places.Autocomplete(input);
	var autocomplete2 = new google.maps.places.Autocomplete(input2);
	//partenza

	autocomplete.addListener('place_changed', function () {
		var place = autocomplete.getPlace();
		$('#latitude').val(place.geometry['location'].lat());
		$('#longitude').val(place.geometry['location'].lng());
		// --------- show lat and long ---------------
		$("#lat_area").removeClass("d-none");
		$("#long_area").removeClass("d-none");
		let latitude = $('#latitude').val();
		let longitude = $('#longitude').val();
		$('#result').attr('hidden', false);
		$('#get-latitude').val(latitude);
		$('#get-longitude').val(longitude);

		/* initMap(); */
	});

	//arrivo
	autocomplete2.addListener('place_changed', function () {
		var place = autocomplete2.getPlace();
		$('#latitude2').val(place.geometry['location'].lat());
		$('#longitude2').val(place.geometry['location'].lng());
		// --------- show lat and long ---------------
		$("#lat_area").removeClass("d-none");
		$("#long_area").removeClass("d-none");
		let latitude = $('#latitude2').val();
		let longitude = $('#longitude2').val();
		$('#result').attr('hidden', false);
		$('#get-latitude').val(latitude);
		$('#get-longitude').val(longitude);

		initMap();
	});


	function initMap() {
		let latitude2 = $('#latitude2').val();
		let longitude2 = $('#longitude2').val();

		let latitude = $('#latitude').val();
		let longitude = $('#longitude').val();
		console.log("lat= " + latitude + " long=" + longitude);

		const partenza = { lat: parseFloat(latitude), lng: parseFloat(longitude) };
		const destinazione = { lat: parseFloat(latitude2), lng: parseFloat(longitude2) };
		/* console.log(frick); */

		let directionsService = new google.maps.DirectionsService();
		let directionsRenderer = new google.maps.DirectionsRenderer();

		const route = {
			origin: partenza,
			destination: destinazione,
			travelMode: 'DRIVING'
		}
		directionsService.route(route,
			function (response, status) { // anonymous function to capture directions
				if (status !== 'OK') {
					window.alert('Directions request failed due to ' + status);
					return;
				} else {
					directionsRenderer.setDirections(response);
					var directionsData = response.routes[0].legs[0]; // Get data about the mapped route
					// Add route to the map
					//console.log(directionsData.distance.value);
					var distancekm = (directionsData.distance.value / 1000);
					$('#alert').text("Distanza in Km: "+distancekm);
					$('#alert').fadeIn();
					$('#distance_km').val(directionsData.distance.value);
					$('#distance_text').val(directionsData.end_address);
					if (!directionsData) {
						window.alert('Directions request failed');
						return;
					}
					console.log(directionsData.end_address);
					let distanzakm = $('#distance_km').val();
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});

					//window.alert( " Driving distance is " + directionsData.distance.text + " (" + directionsData.duration.text + ").");
					/* location.reload(); */
				}
			});
	}
}
    </script>
