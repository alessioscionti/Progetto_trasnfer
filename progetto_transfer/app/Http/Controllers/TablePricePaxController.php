<?php

namespace App\Http\Controllers;

use App\Models\TablePricePax;
use Illuminate\Http\Request;

class TablePricePaxController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(TablePricePax $tablePricePax)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(TablePricePax $tablePricePax)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, TablePricePax $tablePricePax)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(TablePricePax $tablePricePax)
    {
        //
    }
}
